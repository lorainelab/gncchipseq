# GNC Quickload #

The data for this project can be viewed through [IGB](bioviz.org) by using this quickoad site:

[transvar.org/data/schallerlab](https://transvar.org/data/schallerlab)

This module is used to create the annotes.xml which allows IGB to display the files located on the quickload site.  This module does not contain the files to visualize, as they are quite large.