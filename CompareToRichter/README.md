# Compare GNC ChIP-Seq to results from Richter et al GNC data#

We'd like to be able to compare this data to the results in Supp Table S2 from Richter et al's ["The GATA-type transcription factors GNC and GNL/CGA1 repress gibberellin signaling downstream from DELLA proteins and PHYTOCHROMEINTERACTING FACTORS"](https://www.ncbi.nlm.nih.gov/pubmed/20844019). Their raw data is [here](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE21256).

The simplest way to look at their results is to use their table, however the gene column included in the table does not always use AGI codes, sometimes it uses gene symbols.  In order to link to genes (and subsequently to peaks) we can use the mapping data (the genomic region that matches the probe sequence).  

We are using the mapping information from GPL12621.
According to the GEO info for this study ([GSE21256](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE21256)) , the platform used was [GPL9020](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL9020) (which fits what they say in the paper).  That platform lists [GPL12621](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL12621) as an alternative (it has the same title and probe ids, so it also fits).  GPL9020 was first submitted in 2009, and GPL12621 was submitted 2011.  Although both have been updated since then, I suspect the updates have to do with links and meta data, not the main table.  I suspect the GPL9020 mappings (and the mappings in Richter table S2) are for an older A. thaliana genome assembly, while GPL12621 matches the TAIR 2009 assembly and TAIR10 gene annotations that we are using.

for example: probe "A_84_P801431" was given geneSymbol "AT1G68250", and its description ends with "[AK227000]" (I don't recognize that id type).  The mapping from table S2 "chr1:25583526-25583585" falls outside of AT1G68250 by a little over 1 kb (using the TAIR10 gene annotations in IGB).  The mapping given in GPL9020 is the same.  Hover the mapping given in GPL12621 is "chr1:25579863-25579922" which maps within AT1G68250.  Both platforms give the same sequence.  The sequence shown in IGB (using the June 2009 At assembly) matches the probe sequence only at the GPL12621 mapping location.    So I think we should use the mapping information given by the GPL12621 platform, rather than the map column from table S2.

Table S2 includes a probe id.  We can connect probe id to genome location using GPL12621.  Link genome location to TAIR10 gene id using bedtools intersect.  Link peaks to genes using the closest gene table (the same table Eric attached earlier).
Thus, we link the data from Table S2 to the Schaller GNC peaks.

This process is broken into two parts:
In ProbesToGenes.Rmd, connect the probes to TAIR10 genes.
In ProbesToPeaks.Rmd use the probe-to-gene connection to connect to peaks, and make some visuals.

* * *

# Questions? 

Contact:

* Ann Loraine aloraine@uncc.edu
* Ivory Blakley ieclabau@uncc.edu
* Eric Schaller george.e.schaller@dartmouth.edu

* * *

# License

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT