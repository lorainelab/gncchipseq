# Intermediate Files

This folder contains files that were produced in this module but are not the final product.  They are saved for reference along the way, but they are not version controlled.  The final product(s) of the module will be in the results folder, and version controlled.



## What's here

* * * 

### SummitRegions_<sample>.bed

Bed files representing the peaks from each sample.  These files can be view in IGB. They represent the region of each qualifying peak that was used to generate consensus peaks.  See DefineSummitRegions.html.

* * *

### SummitRegions_all.bed

Single bed file containing all of the summit regions. Essentially all of the SummitRegions_<sample>.bed represented in a single file.

* * *

### SummitRegions_merged.bed

This bed file is the product of merging together all overlapping regions from SummitRegions_all.bed.  This includes regions that only have a single peak, and regions that have peaks from more than one sample.  A subset of these will be consensus peaks.

* * *

### ConsensusPeakRegions.bed

Bed file describing consensus peaks. This only includes the range and name of the consensus peak.  This file will be augmented in PeakInfoForConsenusPeaks.Rmd to include more information, and those results will be saved (and version controlled) in the results folder.

