#!/bin/bash

#  MakeLinks.sh
#  
#
#  Created by Ivory on 3/4/15.
#
#
# Fastq files generally come with long, cryptic names.
# Rather than change the name of the file, I'm going to make a series of symbolic links.
# The link will use a convenient nickname that matches the name used for all downstream files.
# The original file will keep its original name and the link (as will as the table used to make the links)
# will be a record of the transition between the original files and my file nicknames.
#
# Use this script in conjuention with a table of original file names to nicknames, to create these links.
# Run it from the directory where you want the links to be made (ie, the nicknames directory).
#


for i in {2..5} #insert correct number
do
    NICKNAME=$(sed -ne "${i}p" ../FastqNickNames.txt | cut -f2)
    FILENAME=$(sed -ne "${i}p" ../FastqNickNames.txt | cut -f1)
    ln -s ../$FILENAME $NICKNAME
done
