#!/bin/bash

#  callRunMACS.sh
#
#  Created by Ivory on 11/04/14.
#

# This is one of three components: callRunMACS.sh, runMACS.pbs, and MACStable.txt
# all three of these files (and the sample files) are necisary to run MACS.

FilePath=../bowtie_alignments

for i in {2..3}
  do
    IP=$(sed -ne "${i}p" MACStable.txt | cut -f1)
    INPUT=$(sed -ne "${i}p" MACStable.txt | cut -f2)
    NAME=$(sed -ne "${i}p" MACStable.txt | cut -f3)

    IPfile=$FilePath/$IP.bam
    INPUTfile=$FilePath/$INPUT.bam

    export IPfile INPUTfile NAME
    qsub -v IPfile,INPUTfile,NAME -e logs/$NAME.log -o logs/$NAME.log -N $NAME runMACS.pbs
  done

