#!/bin/bash
# module load fastqc
dir="fastqc"
if ! [ -e $dir ];
then
    mkdir $dir
fi
files=`ls *.gz | grep -v GES`
for file in $files
do
    cmd="fastqc -t 20 -o $dir --noextract -f fastq $file"
    echo "starting: $cmd"
    $cmd
done