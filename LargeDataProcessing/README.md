# Large Data Processing #

The processing steps that require large data files (the original fastq files and the aligned bam files) are documented here.

# What's here #

* * *

## Scripts

For most steps, there is a <doStuff>.pbs script which calls a given program (such as bowtie) on a single sample, and a call<DoStuff>.sh script that passes each sample or set of samples to doStuff.  Some scripts also need to reference a text file.

* * *

## DirectoryStructure

A text file displaying the file tree from the machine where these steps were performed.  This is helpful in seeing how the scripts relate to each other.
