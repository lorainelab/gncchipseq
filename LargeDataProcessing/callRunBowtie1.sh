#!/bin/bash

# Run this script from the alignment directory.

SRC="."
FILES=$(ls ../fastq/nicknames/*.fastq*)

for FASTQ in $FILES
do
  export FASTQ
  SAMPLE=${FASTQ%.fastq*}
  SAMPLE=${SAMPLE#../fastq/nicknames/}
  export SAMPLE
  qsub -e logs/$SAMPLE.log -o logs/$SAMPLE.log -v FASTQ,SAMPLE -N align-$SAMPLE $SRC/runBowtie1.pbs
done

