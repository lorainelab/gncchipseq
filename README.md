# GNC ChIP-Seq #

This repository contains data files and source code used to analyze
results from a ChIP-Seq experiment in which ...

* * *

# Repository organization

Related analysis and data processing code are grouped into folders -
modules.

Module names indicate the general purpose of the analysis. Each module
runs as a relatively self-contained project in RStudio. As such, each
folder contains an ".Rproj" file. To run the code in RStudio, just
open that file and go from there.

Some modules depend on the output of other modules. Some modules also
depend on the presence of externally supplied data files, which are
version-controlled here but may also be available from external sites.

This project is a collaboration between the Schaller Lab at Dartmouth,
the Kieber Lab at UNC Chapel Hill, and the Loraine Lab at the North
Carolina Research Campus (and UNC Charlotte).

* * *

# What's here #

Analysis modules and other directories include:

* * * 

## ConsensusPeaks

This module selects high confidence peaks from each sample and produces a single set of "consensus peaks", regions that contain a summit from each sample.

* * *

## CompareToARR10ChIP

In this module, we incorporate data from the ARR10 ChIP-Seq project.

* * *

## ExternalDataSets

Datasets obtained from third-party sites.

* * *

## FastQC

Zipped files containing results from running FastQC quality
assessment methods.

* * *

## LargeDataProcessing

Documentation of basic data processing steps such as alignment and peak calling.  This includes the scripts that were used and logs and some summary documentation.  The input files for these steps are not stored here.

* * *

## MACS2_Peaks

The primary output of running MACS2 (.xls files).  
Scripts, logs, and quality checks from running MACS2 are part of the LargeDataProcessing folder.

* * *

## PeaksToGenes

This module links the consensus peaks to the nearest annotated genes.  Peaks are associated with the single closest gene on each strand (up to a set distance limit) and additionally to genes that the peak is internal to.

* * *

## Quickload

Information for and about the quickload site for this project.

* * *

## src

Scripts used in multiple modules.  Scripts that are used exclusively by a single module are in an src folder within that module.  Usually R code. 

* * *

# Questions? 

Contact:

* Ann Loraine aloraine@uncc.edu
* Ivory Blakley ieclabau@uncc.edu
* Eric Schaller george.e.schaller@dartmouth.edu

* * *

# License

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT