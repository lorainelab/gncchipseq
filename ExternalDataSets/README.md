# GNC ChIP-Seq #



## ExternalDataSets

Datasets obtained from third-party sites.

TAIR10.bed.gz is obtained from http://igbquickload.org/A_thaliana_Jun_2009/

* * *

Datasets not stored in the repository.  These publicly available files are downloaded as needed by scripts.

GPL12621-11752.txt is obtained from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL12621.

RichterSuppTableS2.xls is obtained from http://genesdev.cshlp.org/content/24/18/2093/suppl/DC1

* * *

# Questions? 

Contact:

* Ann Loraine aloraine@uncc.edu
* Ivory Blakley ieclabau@uncc.edu
* Eric Schaller george.e.schaller@dartmouth.edu

* * *

# License

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT